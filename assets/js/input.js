(function($){

	Date.prototype.getFullMinutes = function () {
	   if (this.getMinutes() < 10) {
	       return '0' + this.getMinutes();
	   }
	   return this.getMinutes();
	};

	Date.prototype.getFormattedTime = function () {
	    var hours = this.getHours() == 0 ? "12" : this.getHours() > 12 ? this.getHours() - 12 : this.getHours();
	    var minutes = (this.getMinutes() < 10 ? "0" : "") + this.getMinutes();
	    var ampm = this.getHours() < 12 ? "AM" : "PM";
	    var formattedTime = hours + ":" + minutes + " " + ampm;
	    return formattedTime;
	}

	Date.prototype.getFormattedDate = function () {
		var dateInit = $.datepicker.formatDate('M d', this);
		var day = this.getDay();
		var suffix;
		switch(day) {
            case '1': case '21': case '31': suffix = 'st'; break;
            case '2': case '22': suffix = 'nd'; break;
            case '3': case '23': suffix = 'rd'; break;
            default: suffix = 'th';
        }
        var formattedDate = dateInit + suffix;
	    return formattedDate;
	}

	Date.prototype.getFormattedDisplayDateToJS = function () {
		var date = $.datepicker.formatDate('dd/mm/yy', this);
	    return date;
	}

	Date.prototype.getFormattedDateToJS = function () {
		var date = $.datepicker.formatDate('yy-mm-dd', this);
			return date;
	}

	Date.prototype.getFormattedTimeToJS = function () {
		var hours = (this.getHours() < 10 ? "0" : "") + this.getHours();
	    var minutes = (this.getMinutes() < 10 ? "0" : "") + this.getMinutes();
	    var formattedTime = hours + ":" + minutes;
	    return formattedTime;
	}

	function DropDown(el, namespace, data, title) {
		this.dd = el;
		this.title = title;
		this.dropClass = $(this.dd.find('.dropdown'));
		this.namespace = namespace;
		this.placeholder = this.dd.children('span');
		this.setData(data);
		this.required = false;
		this.state = true;
		this.disabledTip = 'This is not yet active';
		// this.fireEvent();
		// this.setData();
	}

	DropDown.prototype = {
		initEvents : function() {
			var obj = this;

			$('html').click(function() {
				if ($(obj.dd).hasClass('active')) {
					$(obj.dd).toggleClass('active');
				}
			});

			// obj.dd.on('mouseover' , function (Event) {
			// 	console.log('TEST');
			// 	// this.tooltip('show');
			// 	// setTimeout(function(){
			//  //        this.tooltip('hide');
			//  //    }, 1000);
			// });

			obj.dd.on('click', function(event){
				//console.log('clicked obj');
				event.stopPropagation();
				$(this).toggleClass('active');
				return false;
			});

			obj.dd.on('focusout', function(event){
				//console.log('focusout');
				if ($(this).hasClass('active')) {
					$(this).toggleClass('active');
				}
			});

			obj.dd.on('blur', function(event){
				//console.log('blur');
				if ($(this).hasClass('active')) {
					$(this).toggleClass('active');
				}
			});

			obj.opts.on('click',function(){
				//console.log('clicked opts');
				var opt = $(this);
				obj.val = opt.text();
				obj.index = opt.index();
				obj.placeholder.text(obj.val);
				obj.fireEvent('changed', opt.data(), obj.val);
			});
		},
		unbindEvents: function () {
			var obj = this;
			obj.dd.unbind();
			this.dd.find('.dropdown a').unbind();
		},
		getValue : function() {
			return this.val;
		},
		getIndex : function() {
			return this.index;
		},
		fireEvent : function(name, data, val) {
			switch (name) {
				case 'changed':
					$( document ).trigger( "changed." + this.namespace, [ data, val ] );
					break;
			}
		},
		disable : function () {
			this.state = false;
			//console.log('DISABLED');
			if (!this.dd.hasClass('disabled')) {
				this.dd.addClass('disabled');
			}
		},
		enable: function () {
			this.state = true;
			//console.log('ENABLED');
			if (this.dd.hasClass('disabled')) {
				this.dd.removeClass('disabled');
			}
		},
		setDisabledTip: function (text) {
			var obj = this;
			obj.disabledTip = text;
			obj.prop('title', obj.disabledTip);
			obj.tooltip('show');
		},
		setData: function (data) {
			var obj = this;
			var objDropClass = this.dropClass;
			//console.log('set data');
			this.unbindEvents();
			objDropClass.find('li').remove().end();
			this.enable();
			$.each(data, function(key, val) {
				//console.log('set data item');
				var aEl = $("<a>").attr('href', '#').text(val.text).data('id', val.data.id);
				if (val.enabled !== null && val.enabled == false) {
					aEl.attr('disabled', true);
					aEl.addClass('disabled');
				}
				var liEl = $("<li>").append(aEl);
				objDropClass.append(liEl);
		  	});
		  	this.opts = this.dd.find('.dropdown a');
			this.val = '';
			this.index = -1;
			this.initEvents();
		  	// initEvents();
		},
		unsetData: function () {
			var obj = this;
			var objDropClass = this.dropClass;
			// console.log('unset data');
			this.unbindEvents();
			objDropClass.find('li').remove().end();
			this.opts = this.dd.find('.dropdown a');
			this.placeholder.text(this.title);
			this.val = '';
			this.index = -1;
			this.initEvents();

		},
		setRequired: function (required) {
			this.required = required;
			this.setRequiredEvent();
		},
		setRequiredEvent: function () {
			if (typeof(this.required) === 'string' || this.required instanceof String) {
				var obj = this;
				$( document ).on( "changed." + this.required, function(e, data, name) {
					//console.log('required event triggered on '+this.namespace);
					obj.unsetData();
					obj.disable();
					$(this).trigger( "changed_required." + obj.namespace, [ data ]);
				});
			}
		}
	}

	var trackDropdown;
	var timeDropdown;
	var runnerDropdown;
	var $courses = [];
	var $course;
	var $courseTimes = [];
	var $races = [];
	var $race;
	var $runners = [];
	var $runner;
	var state = [];
	var $elPostTitle,
		$elRaceID,
		$elName,
		$elTime,
		$elRaceTrack,
		$elSilkUrl,
		$elOdds,
		$elBetfairBSSID,
    $elBetfairBSMID,
    $elBet365OID,
		$elBetvictorOID,
		$elBetwayOID,
		$elRunnerID
    function initialize_field($el) {
        //$el.doStuff();
    }

	function getFieldSelectors($el)
	{
		var elBasetipAutofill = $('body').find('.acf-field-basetip-autofill');
		var parent = elBasetipAutofill.closest('.acf-fields');
		$elRaceID = parent.find("[data-name='race_id']").find('input');
		$elPostTitleHelpText = $('body').find('#title-prompt-text');
		$elPostTitle = $('body').find('#title');
		$elName = parent.find("[data-name='name']").find('input');
		$elDisplayTime = parent.find("[data-name='time']").find('.input');
		$elTime = parent.find("[data-name='time']").find('.input-alt');	//correct time format to be saved in db see ACF datetime
		$elRaceTrack = parent.find("[data-name='race_track']").find('input'); //SHOULD BE RACE COURSE
		$elRaceTrackID = parent.find("[data-name='race_track_id']").find('input');
		$elSilkUrl = parent.find("[data-name='silk_url']").find('input');
		$elOdds = parent.find("[data-name='odds']").find('input');
    $elRunnerID = parent.find("[data-name='runner_id']").find('input');
    $elBet365OID = parent.find("[data-name='bet365_oid']").find('input');    
		$elBetfairBSSID = parent.find("[data-name='bssid']").find('input');
		$elBetfairBSMID = parent.find("[data-name='bsmid']").find('input');
		$elBetvictorOID = parent.find("[data-name='betvictor_oid']").find('input');
		$elBetwayOID = parent.find("[data-name='betway_oid']").find('input');
  
	}

	function saveState()
	{
		var stateItem = [];
		stateItem = [
			{ element: $elRaceID, value: $elRaceID.val() },
			{ element: $elPostTitle, value: $elPostTitle.val() },
			{ element: $elName, value: $elName.val() },
			{ element: $elTime, value: $elTime.val() },
			{ element: $elRaceTrack, value: $elRaceTrack.val() }, //should be RaceCourse
			{ element: $elRaceTrackID, value: $elRaceTrackID.val() }, //should be RaceCourse
			{ element: $elSilkUrl, value: $elSilkUrl.val() },
			{ element: $elOdds, value: $elOdds.val() },
			{ element: $elRunnerID, value: $elRunnerID.val() },
			{ element: $elBetvictorOID, value: $elBetvictorOID.val() },
      { element: $elBetwayOID, value: $elBetwayOID.val() },
      { element: $elBet365OID, value: $elBet365OID.val() },
      { element: $elBetfairBSMID, value: $elBetfairBSMID.val() },
			{ element: $elBetfairBSSID, value: $elBetfairBSSID.val() },
      { element: $elBetfairBSMID, value: $elBetfairBSMID.val() },
		];
		state = stateItem;
	}

	function autoFillBaseTips(raceID) {
		var race = $races[raceID];
		$elTime.val(race.date_time);
		$elRaceTrack.val(race.course.name);
	}

	function autoFillTrackSelection(course) {
		$elRaceTrack.val(course.name);
		$elRaceTrackID.val(course.timeform_id); //fills custom fields with data
	}

	function autoFillTimeSelection(race){
		$elRaceID.val(race.timeform_id);
		var dateObj = new Date(race.start_datetime_formatted);
		displayDateFormat = dateObj.getFormattedDisplayDateToJS();
		dateFormat = dateObj.getFormattedDateToJS();
  	timeFormat = dateObj.getFormattedTimeToJS();
		$elDisplayTime.val(displayDateFormat + ' ' + timeFormat);
		$elTime.val(dateFormat + ' ' + timeFormat);
	}
  function clearOddsValues() {
    $($elBetfairBSSID).val('')
    $($elBetfairBSMID).val('')
    $($elBet365OID).val('')
    $($elBetvictorOID).val('')
    $($elBetwayOID).val('')
  }
	function autoFillRunner(runner) {
    clearOddsValues();
		if(typeof runner.id  != 'undefined'){
			$elRunnerID.val(runner.timeform_id);
		}else{
			alert('ERROR: Can not retreive runner id from API, this will not load dynamic odds');
		}
		$elSilkUrl.val((typeof runner.silk_url  != 'undefined') ? runner.silk_url : '');
		$elPostTitleHelpText.addClass('screen-reader-text'); // to hide default 'placeholder' appearing inside Post Title
		$elPostTitle.val((typeof runner.name  != 'undefined') ? runner.name : '');
		$elName.val((typeof runner.name  != 'undefined') ? runner.name : '');
		if(runner.odds.length > 0){

			$.each(runner.odds, function( index, value ) {
				// console.log(value);
				if(index==0){
					//odds should be ordered by best odds
					// console.log("selected best odds");
					$elOdds.val((value.fractional_odds != 'undefined') ? value.fractional_odds : 'SP');
				}
				switch (value.bookmaker) {
          case "betfair":
          	$elBetfairBSSID.val((typeof value.params.bf_selection_id != 'undefined') ? value.params.bf_selection_id : '');
          	$elBetfairBSMID.val((typeof value.params.bf_market_id != 'undefined') ? value.params.bf_market_id : '');
            break;
          case "betway":
          	$elBetwayOID.val((typeof value.params.outcome_id != 'undefined') ? value.params.outcome_id : '');
            break;
          case "betvictor":
          	$elBetvictorOID.val((typeof value.params.outcome_id != 'undefined') ? value.params.outcome_id : '');
            break;
          case "bet365":
            $elBet365OID.val((typeof value.params.outcome_id != 'undefined') ? value.params.outcome_id : '');
            break;
				}
			});

		}else{
			alert('There is no odds available for this runner at the moment.');
		}


	}

	function toggleLoading($el, show){
		if(show){
			$el.addClass('select-loading');
		}else{
			$el.removeClass('select-loading');
		}
	}

	function initRaceSelections(){
		$el = $('#basetips_autofill_track_selection');
		toggleLoading($el, true);
		var todaysDate = new Date();
		todaysDate = todaysDate.getFormattedDateToJS();
		var tomorrowsDate = new Date();
		tomorrowsDate.setDate(tomorrowsDate.getDate()+1);
		tomorrowsDate = tomorrowsDate.getFormattedDateToJS();
		console.log(todaysDate);
		console.log(tomorrowsDate);
		jQuery.ajax({
			url : posturl.ajax_url,
			type : 'post',
			data : {
				action : 'getCoursesWithRacesBetweenDates',
				fromDate:todaysDate,
				toDate:tomorrowsDate
			},
			success : function( response ) {
				if (response.success) {
					// console.log("RESPONSE");
				  // console.log(response);
					fillRaceSelections(response.data);
				} else {
					//show error
					console.log("ERROR - Response:");
					console.log(response);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				$( '#basetips_autofill_track_selection' ).tooltip('show');
				setTimeout(function(){
			        $( '#basetips_autofill_track_selection' ).tooltip('hide');
			    }, 5000);
		    },
		    complete: function() {
		    	toggleLoading($el, false);
		    }
		});
	}

	function call_post_get_race_selections(attempt){

		$.ajax({
			url : posturl.ajax_url,
			type : 'post',
			data : {
				action : 'getCoursesWithRacesBetweenDates'
			},
			success : function( response ) {
				if (response.success) {
					fillRaceSelections(response.data);
					$( '#basetips_autofill_track_selection' ).tooltip('hide');
					// console.log(response);
				} else {
					//show error
					console.log(response);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Status: " + textStatus);
				console.log("Error: " + errorThrown);
			    if (attempt < 3) {
					$( '#basetips_autofill_track_selection' ).prop('title', 'There was an error retrieving Race Tracks. Attempting to retry...'+attempt);
					$( '#basetips_autofill_track_selection' ).tooltip('show');
					call_post_get_race_selections(++attempt);
					setTimeout(function(){
				        $( '#basetips_autofill_track_selection' ).tooltip('hide');
				    }, 3000);
				} else {
					$( '#basetips_autofill_track_selection' ).prop('title', 'The server could not be reached, please contact admin@celfcreative.com');
					$( '#basetips_autofill_track_selection' ).tooltip('show');
				}
		    },
		    complete: function() {
		    	toggleLoading($el, false);
		    }
		});
	}

	function fillRaceSelections(data){

		$courses = data;
		// 	$courseTimes = data.courses_times;
		// $races = data.races;

		console.log('COURSES POPULATED');
		// console.log($courses);

		var dropDownData = [];
		$.each(data, function(key, val) {
			// console.log(val);
			dropDownData.push({text: val.name, enabled: 1, data: {id: val.id}});
	  	});
	  	trackDropdown = new DropDown( $('#basetips_autofill_track_selection'), 'track_selection', dropDownData, 'Race Track');
	}

	function initTimeSelections(course){
		$el = $('#basetips_autofill_time_selection');
		toggleLoading($el, true);
		fillTimeSelections(course);
		toggleLoading($el, false);
	}

	function fillTimeSelections(course){
		// console.log(course.races);
		//LOOP THROUGH RACES AND POPULATE TIMES
		var dateObj, dateFormat, timeFormat;
		var dropDownData = [];
		$.each(course.races, function(ctKey, ctVal) {
			dateObj = new Date(ctVal.start_datetime_formatted);
			dateFormat = dateObj.getFormattedDate();
			timeFormat = dateObj.getFormattedTime();
			dropDownData.push({text: dateFormat + ' - ' + timeFormat, enabled: 1, data: {id: ctVal.id}});
		});
  	timeDropdown = new DropDown( $('#basetips_autofill_time_selection'), 'time_selection', dropDownData, 'Time' );
  	timeDropdown.setRequired('track_selection');

	}

	function initRunnerSelections(id){
		$el = $('#basetips_autofill_runner_selection');
		toggleLoading($el, true);
		call_post_get_racecard_odds(id, 0);
	}

	function call_post_get_racecard_odds(id, attempt){

		$.ajax({
			url : posturl.ajax_url,
			type : 'post',
			data : {
				action : 'getRaceWithRunners',
				race_id : id
			},
			success : function( response ) {
				if (response.success) {
					console.log("ajax_get_race");
					fillRunnerSelections(response);
					$( '#basetips_autofill_runner_selection' ).tooltip('hide');
				} else {
					//show error
					console.log("error_ajax_get_race");
					console.log(response);
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log("Status: " + textStatus);
				console.log("Error: " + errorThrown);
				if (attempt < 3) {
					$( '#basetips_autofill_runner_selection' ).prop('title', 'There was an error retrieving Race Tracks. Attempting to retry...'+attempt);
					$( '#basetips_autofill_runner_selection' ).tooltip('show');
					call_post_get_racecard_odds(id, success, ++attempt);
					setTimeout(function(){
				        $( '#basetips_autofill_runner_selection' ).tooltip('hide');
				    }, 3000);
				} else {
					$( '#basetips_autofill_runner_selection' ).prop('title', 'The server could not be reached, please contact admin@celfcreative.com');
					$( '#basetips_autofill_runner_selection' ).tooltip('show');
				}
		    },
		    complete: function() {
		    	toggleLoading($el, false);
		    }
		});
	}

	function fillRunnerSelections(response)
	{

		$runners = response.data[0].runners;
		// console.log($runners);
		var dropDownData = [];
	  	$.each($runners, function(key, val) {
	  		dropDownData.push({text: val.name, data: {id: val.id}});
	  	});
	  	runnerDropdown = new DropDown( $('#basetips_autofill_runner_selection'), 'runner_selection', dropDownData, 'Horse');
	  	runnerDropdown.setRequired('time_selection');
	}


	if( typeof acf.add_action !== 'undefined' ) {

		/*
		*  ready append (ACF5)
		*
		*  These are 2 events which are fired during the page load
		*  ready = on page load similar to $(document).ready()
		*  append = on new DOM elements appended via repeater field
		*
		*  @type	event
		*  @date	20/07/13
		*
		*  @param	$el (jQuery selection) the jQuery element which contains the ACF fields
		*  @return	n/a
		*/

		acf.add_action('ready append', function( $el ){

			// search $el for fields of type 'basetip_autofill'
			acf.get_fields({ type : 'basetip_autofill'}, $el).each(function(){

				initialize_field( $(this) );

			});

		});

		acf.add_action('ready', function( $el ){

			initRaceSelections();
			getFieldSelectors($el);
			saveState();

			$( document ).on( "changed.track_selection", function(e, data, name) {
				// console.log('changed.track_selection');
				// console.log(data); //id passed
				//SET SELECTED COURSE TO $COURSE GLOBAL VAR
				$.each($courses, function( key, val ) {
					if(val.id==data.id){
						$course = val;
					}
				});
				console.log('COURSE SELECTED');
				console.log($course);
				autoFillTrackSelection($course);
		    initTimeSelections($course);

			});

			$( document ).on( "changed_required.track_selection", function(e, data) {
				console.log('changed_required.track_selection');
				//SET SELECTED COURSE TO $COURSE GLOBAL VAR
				$.each($courses, function( key, val ) {
					if(val.id==data.id){
						$course = val;
					}
				});
				console.log('COURSE SELECTED');
				console.log($course);
				autoFillTrackSelection($course);
				initTimeSelections($course);
			});

			$( document ).on( "changed.time_selection", function(e, data, name) {
				console.log('changed.time_selection');
				$.each($course.races, function( key, val ) {
					if(val.id==data.id){
						$race = val;
					}
				});
				autoFillTimeSelection($race);
				initRunnerSelections($race.id);
			});

			$( document ).on( "changed_required.time_selection", function(e, data) {
				console.log('changed_required.time_selection');
				$.each($courses, function( key, val ) {
					if(val.id==data.id){
						$course = val;
					}
				});
				initTimeSelections($course);
				runnerDropdown.unsetData();
				runnerDropdown.disable();
			});

			$( document ).on( "changed.runner_selection", function(e, data, name) {
				console.log('changed.runner_selection');
				// console.log(data);
				$.each($runners, function(key, val) {
					if(val.id==data.id){
						$runner = val;
					}
		  	});
				autoFillRunner($runner);
			});

			$( document ).on( "changed_required.runner_selection", function(e, data) {
				console.log('changed_required.runner_selection');
				initRunnerSelections($race.id);
			});

		});

	} else {


		/*
		*  acf/setup_fields (ACF4)
		*
		*  This event is triggered when ACF adds any new elements to the DOM.
		*
		*  @type	function
		*  @since	1.0.0
		*  @date	01/01/12
		*
		*  @param	event		e: an event object. This can be ignored
		*  @param	Element		postbox: An element which contains the new HTML
		*
		*  @return	n/a
		*/

		$(document).on('acf/setup_fields', function(e, postbox){

			$(postbox).find('.field[data-field_type="basetip_autofill"]').each(function(){

				initialize_field( $(this) );

			});

		});


	}


})(jQuery);
