### Staging status ###
[![Deployment status from DeployBot](https://celf-creative.deploybot.com/badge/77558060110430/99501.svg)](https://celf-creative.deploybot.com/incidents)

# ACF Tips Autofill Plugin #

Main hook between the api and the site. Custom select group which autofills the selection ids for a tip(a horse) in order to bring through odds on the site.


Requires a specific layout of the custom field group.