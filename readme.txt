=== Advanced Custom Fields: Base Tip AutoFill Field ===
Contributors: Celf Creative
Tags: #
Requires at least: 3.5
Tested up to: 3.8.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

SHORT_Automatically fill a Base Tip using the Race ID to search Betfair Feeds

== Description ==

EXTENDED_Automatically fill a Base Tip using the Race ID to search Betfair Feeds

= Compatibility =

This ACF field type is compatible with:
* ACF 5
* ACF 4

== Installation ==

1. Copy the `acf-basetip_autofill` folder into your `wp-content/plugins` folder
2. Activate the Base Tip AutoFill plugin via the plugins admin page
3. Create a new field via ACF and select the Base Tip AutoFill type
4. Please refer to the description for more info regarding the field type settings

== Changelog ==

= 1.0.0 =
* Initial Release.
